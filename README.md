#### K8 Authentication
Kubernetes works on certificate and key based authentication. When kubernetes cluster is created there will be Root CA and key files created in /etc/kubernetes/pki folder. <br/>
Admins use these root CA to sign the user CSR files. <br/>

1. First we need to create private key for the user.
```
openssl genrsa -out <name.key> 2048
```
2. Next we need to create CSR for the private key. /O info is very important it denotes group name.
```
openssl req -new -key <name.key> -out <name.csr> -subj '/CN=<user-name>/O=<group-name>'
```
3. Sign the generated CSR with k8 Root CA and Key

CA_LOCATION usually /etc/kubernetes/pki. you can copy these from K8 cluster and keep in some secure folder outside of K8 cluster.
```
openssl x509 -req -in <name>.csr -CA CA_LOCATION/ca.crt -CAkey CA_LOCATION/ca.key -CAcreateserial -out <name>.crt -days 500
```

Create the Role and Rolebindings. You can create kubernetes config for the user with crt, key, cluster certificate, server URL