FROM jenkins
USER root
RUN apt update -y
RUN apt install maven -y
RUN mvn --version
USER jenkins